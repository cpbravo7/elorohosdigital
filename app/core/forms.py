from django.forms import *

from core.models import Especialidad


class EspecialidadForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'
            self.fields['nombre'].widget.attrs['autofocus'] = True

    class Meta:
        model = Especialidad
        fields = '__all__'
        #  Widgets me permite personalizar mis componentes,
        #  campos de relleno del formulario.
        widgets = {
            'nombre': TextInput(
                # Attrs permite aumentar atributos
                # a los campos
                attrs={
                    # 'class': 'form-control', Propiedades de los campos
                    # de relleno extensivos
                    'placeholder': 'Ingrese un nombre',

                },
            ),
            'descripcion': Textarea(
                attrs={
                    'placeholder': 'Ingrese la descripción',
                    'rows': '3',
                    'columns': '3'
                }
            )

        }
