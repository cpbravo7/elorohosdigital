# Generated by Django 4.1.4 on 2023-01-10 00:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Especialidad',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50, unique=True)),
                ('descripcion', models.CharField(blank=True, max_length=300, null=True)),
                ('fecha_registro', models.DateTimeField(auto_now=True, null=True, verbose_name='Fecha de Registro')),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True, verbose_name='Fecha de Actualización')),
                ('persona_registro', models.CharField(blank=True, max_length=50, null=True, verbose_name='Usuario de Registro')),
                ('persona_actualizacion', models.CharField(blank=True, max_length=50, null=True, verbose_name='Usuario de Actualización')),
                ('estado_especialidad', models.BooleanField(blank=True, default=True, null=True, verbose_name='Activo')),
            ],
            options={
                'verbose_name': 'Especialidad',
                'verbose_name_plural': 'Especialidades',
                'db_table': 'Especialidad',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombres', models.CharField(max_length=255, verbose_name='Nombres')),
                ('apellidos', models.CharField(max_length=255, verbose_name='Apellidos')),
                ('identificacion', models.PositiveIntegerField(unique=True, verbose_name='Dni')),
                ('telefono', models.PositiveIntegerField(unique=True, verbose_name='Teléfono')),
                ('ciudad_nacimiento', models.CharField(max_length=255)),
                ('ciudad_residencia', models.CharField(max_length=255)),
                ('genero', models.CharField(max_length=15)),
                ('usuario_hosdigital', models.CharField(max_length=50, verbose_name='Usuario Hosdigital')),
                ('especialidad', models.ManyToManyField(to='core.especialidad')),
            ],
            options={
                'verbose_name': 'Persona',
                'verbose_name_plural': 'Personas',
                'db_table': 'Persona',
                'ordering': ['id'],
            },
        ),
    ]
