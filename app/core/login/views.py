from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import render, redirect


class LoginFormView(LoginView):
    template_name = 'login.html'

    # Mantener la sesion abierto en caso este logueado

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('hosdigital:especialidad_list')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Iniciar Sesión'
        return context
