from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from core.forms import EspecialidadForm
from core.models import Especialidad


# def especialidad_list(request):
#     data = {
#         'title': 'Listado de Especialidades',
#         'especialidades': Especialidad.objects.all()
#     }
#     return render(request, 'especialidad/list.html', data)
#

class especialidadListView(ListView):
    model = Especialidad
    template_name = 'especialidad/list.html'

    # Para sobreescribir el comportamiento de ListView
    # def get_queryset(self):
    #    return Especialidad.objects.filter(nombre__istartswith='G')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Especialidades'
        context['entity'] = 'Especialidades'
        context['create_url'] = reverse_lazy('hosdigital:especialidad_createview')
        context['list_url'] = reverse_lazy('hosdigital:especialidad_list')
        # Alterar el comportamiento de List_View modificando los modelos object_list
        # context['objet_list'] = Persona.objects.all()
        return context


class especialidadCreateView(CreateView):
    model = Especialidad
    form_class = EspecialidadForm
    template_name = 'especialidad/create.html'
    success_url = reverse_lazy('hosdigital:especialidad_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Crear una nueva especialidad'
        context['entity'] = 'Especialidades'
        context['list_url'] = reverse_lazy('hosdigital:especialidad_list')
        context['action'] = 'add'
        return context


class especialidadUpdateView(UpdateView):
    model = Especialidad
    form_class = EspecialidadForm
    template_name = 'especialidad/create.html'
    success_url = reverse_lazy('hosdigital:especialidad_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de especialidad'
        context['entity'] = 'Especialidades'
        context['list_url'] = reverse_lazy('hosdigital:especialidad_list')
        context['action'] = 'edit'
        return context


class especialidadDeleteView(DeleteView):
    model = Especialidad
    template_name = 'especialidad/delete.html'
    success_url = reverse_lazy('hosdigital:especialidad_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de especialidades'
        context['entity'] = 'Especialidades'
        context['list_url'] = reverse_lazy('hosdigital:especialidad_list')
        return context
