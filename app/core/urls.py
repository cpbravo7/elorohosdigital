from django.urls import path
from core.views.especialidad.views import *

# Nombre del bloque de rutas secundarias

app_name = 'hosdigital'

urlpatterns = [
    path('especialidad/list', especialidadListView.as_view(), name='especialidad_list'),
    path('especialidad/create', especialidadCreateView.as_view(), name='especialidad_createview'),
    path('especialidad/edit/<int:pk>/', especialidadUpdateView.as_view(), name='especialidad_updateview'),
    path('especialidad/delete/<int:pk>/', especialidadDeleteView.as_view(), name='especialidad_delete')

 ]
