from django.db import models
from datetime import datetime


class Especialidad(models.Model):
    nombre = models.CharField(max_length=50, unique=True)
    descripcion = models.CharField(max_length=300, null=True, blank=True)
    fecha_registro = models.DateTimeField(auto_now_add=True, null=True, blank=True, verbose_name='Fecha de Registro')
    fecha_actualizacion = models.DateTimeField(auto_now=True, verbose_name='Fecha de Actualización')
    persona_registro = models.CharField(max_length=50, verbose_name='Usuario de Registro', null=True, blank=True)
    persona_actualizacion = models.CharField(max_length=50, verbose_name='Usuario de Actualización', null=True,
                                             blank=True)
    estado_especialidad = models.BooleanField(default=True, verbose_name='Activo', null=True, blank=True)

    def __str__(self):
        return self.nombre

    #    return 'Nro: {} / Nombre: {}'.format(self.id, self.nombre)
    # transfomar numero a string: return str(self.id)

    class Meta:
        verbose_name = 'Especialidad'
        verbose_name_plural = 'Especialidades'
        db_table = 'Especialidad'
        ordering = ['id']


class Persona(models.Model):
    nombres = models.CharField(max_length=255, verbose_name='Nombres')
    apellidos = models.CharField(max_length=255, verbose_name='Apellidos')
    identificacion = models.PositiveIntegerField(unique=True, verbose_name='Dni')
    telefono = models.PositiveIntegerField(unique=True, verbose_name='Teléfono')
    correo_electronico = models.Field
    direccion = models.Field
    ciudad_nacimiento = models.CharField(max_length=255)
    ciudad_residencia = models.CharField(max_length=255)
    fecha_nacimiento = models.DateField
    genero = models.CharField(max_length=15)
    usuario_hosdigital = models.CharField(max_length=50, verbose_name='Usuario Hosdigital')
    # Relacion muchos a muchos
    especialidad = models.ManyToManyField(Especialidad, blank=True)

    # Relacion uno a muchos
    # especialidad = models.ForeignKey(Especialidad, on_delete=models.PROTECT)

    def __str__(self):
        return 'Nombre: {} / Apellido: {}'.format(self.nombres, self.apellidos)

    #     return self.nombres

    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'
        db_table = 'Persona'
        ordering = ['id']

